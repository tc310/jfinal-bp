﻿//加入收藏 
//自动调用写法AddFavorite(window.location,document.title)
function AddFavorite(sURL, sTitle)
    {
        try
        {
            window.external.addFavorite(sURL, sTitle);
        }
        catch (e)
        {
            try
            {
                window.sidebar.addPanel(sTitle, sURL, "");
            }
            catch (e)
            {
                alert("加入收藏失败，请使用Ctrl+D进行添加");
            }
        }
    }

//设为主页
//自动调用写法SetHome(this,window.location)
function SetHome(obj,vrl)
{
    try
    {
            obj.style.behavior='url(#default#homepage)';obj.setHomePage(vrl);
    }
    catch(e){
            if(window.netscape) {
                    try {
                            netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect"); 
                    } 
                    catch (e) { 
                            alert("此操作被浏览器拒绝！\n请在浏览器地址栏输入“about:config”并回车\n然后将[signed.applets.codebase_principal_support]设置为'true'"); 
                    }
                    var prefs = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
                    prefs.setCharPref('browser.startup.homepage',vrl);
             }
    }
}

//全文检索打开详情页
function OpenWindowForDetail(url)
{
	var indexo=-1;
	try{
		var path=parent.window.location;
		path=path.pathname;
		indexo=path.indexOf("infofulltextsearch.seam");
	}catch(err)
	{
		indexo=-1;
	}
	if(indexo!=-1)
	{
		OpenMaxWindow(url);
	}
	else{
		var s1=url.split("?");//s[1]:InfoGuid=aaa&title=bbb
		var s2=s1[1].split("=");
		var guid1=s2[1].split("&");
		var infoguid=guid1[0];
		var title=s2[2];
		url="/SuQian365/frame/fulltextsearch/search/ResultDetail.seam?tableName=webinfo_information&businessGuid="+infoguid+"&title="+title;
		var index=url.indexOf("/frame/");
		url=url.substring(index);
		var title=getParamaterByNameAndUrl("title",url);
		var length=5;
		if(title.length>length)
		{
			title=title.substring(0,length+1)+"...";
		}
		var id=Math.round(Math.random()*100)+"_fulltext";
		 OpenPageOnTabFirst(id, "Images/SmallIcon/ly.gif", title, url);
	}
}

function OpenWindowForAttach(url)
{
	OpenMaxWindow(url);
}



function clearValues(id)
{
	var ele=document.getElementById(id);
	if(ele!=null)
	{
		ele.value='';
	}
}

function getAbsoluteLocation(element) 
{ 
    if ( arguments.length != 1 || element == null ) 
    { 
        return null; 
    } 
    var offsetTop = element.offsetTop; 
    var offsetLeft = element.offsetLeft; 
    var offsetWidth = element.offsetWidth; 
    var offsetHeight = element.offsetHeight; 
    while( element = element.offsetParent ) 
    { 
        offsetTop += element.offsetTop; 
        offsetLeft += element.offsetLeft; 
    } 
    return { absoluteTop: offsetTop, absoluteLeft: offsetLeft, 
        offsetWidth: offsetWidth, offsetHeight: offsetHeight }; 
} 
function myalert(value){
	alert(value);
}
// utf-8编码函数
function EncodeUtf8(s1)  
{  
    var s = escape(s1);  
    var sa = s.split("%");  
    var retV ="";  
    if(sa[0] != "")  
    {  
       retV = sa[0];  
    }  
    for(var i = 1; i < sa.length; i ++)  
    {  
         if(sa[i].substring(0,1) == "u")  
         {  
             retV += Hex2Utf8(Str2Hex(sa[i].substring(1,5)));  
                
        }  
         else retV += "%" + sa[i];  
    }  
       
    return retV;  
}  
function Str2Hex(s)  
{  
    var c = "";  
    var n;  
    var ss = "0123456789ABCDEF";  
    var digS = "";  
    for(var i = 0; i < s.length; i ++)  
    {  
       c = s.charAt(i);  
       n = ss.indexOf(c);  
       digS += Dec2Dig(eval(n));  
            
    }  
    // return value;
    return digS;  
}  
function Dec2Dig(n1)  
{  
    var s = "";  
    var n2 = 0;  
    for(var i = 0; i < 4; i++)  
    {  
       n2 = Math.pow(2,3 - i);  
       if(n1 >= n2)  
       {  
          s += '1';  
          n1 = n1 - n2;  
        }  
       else  
        s += '0';  
           
    }  
    return s;  
       
}  
function Dig2Dec(s)  
{  
    var retV = 0;  
    if(s.length == 4)  
    {  
        for(var i = 0; i < 4; i ++)  
        {  
            retV += eval(s.charAt(i)) * Math.pow(2, 3 - i);  
        }  
        return retV;  
    }  
    return -1;  
}   
function Hex2Utf8(s)  
{  
   var retS = "";  
   var tempS = "";  
   var ss = "";  
   if(s.length == 16)  
   {  
       tempS = "1110" + s.substring(0, 4);  
       tempS += "10" +  s.substring(4, 10);   
       tempS += "10" + s.substring(10,16);   
       var sss = "0123456789ABCDEF";  
       for(var i = 0; i < 3; i ++)  
       {  
          retS += "%";  
          ss = tempS.substring(i * 8, (eval(i)+1)*8);  
             
             
             
          retS += sss.charAt(Dig2Dec(ss.substring(0,4)));  
          retS += sss.charAt(Dig2Dec(ss.substring(4,8)));  
       }  
       return retS;  
   }  
   return "";  
}   
function fGetPEUtf8(sUtf8PE) // %xx[,%xx]
{ // shawl.qiu code, return string
 sUtf8PE = sUtf8PE.replace(/%/, "");
 //IWrite.write("<br/>sUtfPE: "+sUtf8PE);
 var Ar = sUtf8PE.split("%");
 for(var i=0, j=Ar.length; i<j; i++)
 {
  Ar[i] = parseInt(Ar[i], 16).toString(2);
  var iZeroIndex = Ar[i].indexOf("0");
  Ar[i] = Ar[i].slice(iZeroIndex+1);
 }
 var sBin = Ar.join("");
 var iCode = parseInt(sBin, 2);
 return String.fromCharCode(iCode);
} // end function fGetPEUtf8(sUtf8PE)

// PE = percent encoding
function fGetPEUtf8Bound(cUtf8PE) // %xx
{ // shawl.qiu code, return integer
 cUtf8PE = cUtf8PE.replace(/%/, "");
 var iCharCode = parseInt(cUtf8PE, 16);
 var iLBound = iUBound = 0;
 //00-7F
 iLBound = 0; iUBound = 0x7f; 
 if(iCharCode>=iLBound&&iCharCode<=iUBound) return 1;
 //C2-DF
 iLBound = 0xC2; iUBound = 0xDF; 
 if(iCharCode>=iLBound&&iCharCode<=iUBound) return 2;
 //E0-EF
 iLBound = 0xE0; iUBound = 0xEF; 
 if(iCharCode>=iLBound&&iCharCode<=iUBound) return 3;
 //F0-F4
 iLBound = 0xF0; iUBound = 0xF4; 
 if(iCharCode>=iLBound&&iCharCode<=iUBound) return 4;
 
 return 0;
} // end function fGetPEUtf8Bound(cUtf8PE)

function fGeneratingBoundStr(iPadBegin, iLen)
{
 if(!iPadBegin) iPadBegin = 0; 
 if(!iLen) iLen = 10; 
 var Ar = [];
 var iLBound = iUBound = 0;
 
 iLBound = 0+iPadBegin; iUBound = iLBound+iLen; 
 for(;iLBound<iUBound; iLBound++ )
 {
  Ar.push(String.fromCharCode(iLBound));
 }
 
 iLBound = 0x000080+iPadBegin; iUBound = iLBound+iLen;
 //IWrite.write("<br/>iLBound: "+iLBound); 
 for(;iLBound<iUBound; iLBound++ )
 {
  Ar.push(String.fromCharCode(iLBound));
 }
 
 iLBound = 0x000800+19000+iPadBegin; iUBound = iLBound+iLen;
 //IWrite.write("<br/>iLBound: "+iLBound);  
 for(;iLBound<iUBound; iLBound++ )
 {
  Ar.push(String.fromCharCode(iLBound));
 }
 
 iLBound = 0x010000+iPadBegin; iUBound = iLBound+iLen; 
 //IWrite.write("<br/>iLBound: "+iLBound);
 for(;iLBound<iUBound; iLBound++ )
 {
  Ar.push(String.fromCharCode(iLBound));
 }
 
 return Ar;
}


function IDecodeURIUtf(sUtf8PE) // %xx[,%xx]
{ // shawl.qiu code, return string; Func: fGetPEUtf8, fGetPEUtf8Bound
	if(sUtf8PE===undefined)
	{
		return;
	}
 if(sUtf8PE.indexOf("%")===-1) return sUtf8PE;
 sUtf8PE = sUtf8PE.replace(/\+/g, " ");
 
 for(var i=0, j=sUtf8PE.length; i<j; i++)
 {
  var iIndex = sUtf8PE.indexOf("%", i);
  if(iIndex===-1) break;
  i=iIndex+1;
  
  var iBound = fGetPEUtf8Bound(sUtf8PE.slice(i, i+2));    
  switch(iBound)
  {
   case 1:
    var sHexExt = sUtf8PE.slice(i, i+2);
    TempStr = String.fromCharCode(parseInt(sHexExt, 16));
    sUtf8PE = [sUtf8PE.slice(0, i-1), TempStr, sUtf8PE.slice(i+2)].join("");
    i-=1;
    break;
    
   case 2: 
    var sHexExt = sUtf8PE.slice(i+2, i+5);
    if(/%../.test(sHexExt))
    {
     var TempStr = sUtf8PE.slice(i-1, i+5);
     TempStr = fGetPEUtf8(TempStr);
     sUtf8PE = [sUtf8PE.slice(0, i-1), TempStr, sUtf8PE.slice(i+5)].join("");
     i-=1;
    }
    break;
    
   case 3:
    var sHexExt = sUtf8PE.slice(i+2, i+8);
    if(/\%..\%/.test(sHexExt))
    {
     var TempStr = sUtf8PE.slice(i-1, i+8);
     TempStr = fGetPEUtf8(TempStr);
     sUtf8PE = [sUtf8PE.slice(0, i-1), TempStr, sUtf8PE.slice(i+8)].join("");
     i-=1;
    }
    break;
  }
 } // end for(var i=0, j=sUtf8PE.length; i<j; i++)

 return sUtf8PE;
}

function setMainContentWidth(hasTree)
{
	var leftMenu=parent.Td_Look;
	var leftMenuWidth=0;
	if(leftMenu!=null)
	{
		if(leftMenu.style.display!="none")
		{
			leftMenuWidth=leftMenu.offsetWidth;
		}
	}
	var treeWidth=0;
	if(hasTree)
	{
		treeWidth=200;
	}
	var targetDiv=document.getElementById('rightcontent');
	if(targetDiv!=null)
	{
		targetDiv.style.width=window.screen.width-leftMenuWidth-treeWidth-15;
	}
}

function getParamaterByName(name)
{
	var aa=window.location.href;
	return getParamaterByNameAndUrl(name,aa);
}

function getParamaterByNameAndUrl(name,aa)
{
	var parames='';
	var index=aa.indexOf(name+'=');
	if(index>-1)
	{
		parames=aa.substring(index+1+name.length,aa.length);
		if(parames.indexOf('&')!=-1)
		{
			var paramvalue=parames.split('&');
			parames= paramvalue[0];
		}
	}
    return parames;
}

// 验证输入的是否数字
function  validateNumber(txt)
{   
   txt.value=txt.value.replace(/\D/g,"");   
} 

// 验证图片格式
function file_onfocus(id) 
{
	file_onfocusByDiv(id,imagePreview);
}

function file_onfocusByDiv(id,divInner) 
{
	  var imgFile=document.getElementById(id);
	  if(imgFile.value!="")
	  {
		  var mfilename=imgFile.value.substring(imgFile.value.lastIndexOf("."),imgFile.value.length);
		  mfilename=mfilename.toLowerCase();
		  if((mfilename.indexOf(".jpg") != -1)||(mfilename.indexOf(".gif") != -1)||(mfilename.indexOf(".bmp") != -1)||(mfilename.indexOf(".png") != -1))
		  {
			  var path;
			  // IE
			  if(document.all)
			  {
				  imgFile.select();
				  path = document.selection.createRange().text;
				  divInner.innerHTML="";
				  divInner.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled='true',sizingMethod='scale',src=\"" + path + "\")";// 使用滤镜效果
			  }
			  // FF
			  else
			  {
				  path = imgFile.files[0].getAsDataURL();
				  divInner.innerHTML="<IMG style='WIDTH: 120px; HEIGHT: 180px' SRC='file:///" + path + "' />";
			  }
		  }
		  else
		  {
			    //varfilePicture.outerHTML=varfilePicture.outerHTML;
			    alert("图片格式不对,系统允许的类型有：jpg;gif;bmp;png");
			    divInner.innerHTML="";
		  }
	  }
}

// 文本框按回车 触发搜索
function enterForSearch(btnId) {
	// 只考虑IE
	btnEl = document.getElementById(btnId);
	document.attachEvent('onkeydown', function() {
		if( (el = event.srcElement).nodeName == 'INPUT' ) {;
			if(event.keyCode == 13) btnEl.click();
		}
	});
}

// 判断是否选中
function isSelect(msg)
{
	return isSelectWithExcel(msg,false);
}

// 显示元素
function showEle()
{
    document.getElementById('searchArea').style.display='block'; 
}
// 隐藏元素
function hideEle()
{
   document.getElementById('searchArea').style.display='none'; 
}
// 判断显隐
function ShowHide(obj)
{
   if( obj == '打开搜索' )
   {
      document.getElementById('templateform:btnSearch').value = '关闭搜索';
   }
   else
   {
     document.getElementById('templateform:btnSearch').value = '打开搜索';
   }
   if(document.getElementById('searchArea').style.display=='block')
   {
     hideEle();
   }
   else
   {
    showEle();
   }
}

function checkDataTableSelectDefault(operate,needConfirm)
{
	return checkDataTableSelectAll(dataTable,operate,"记录",needConfirm);
}


function checkDataTableSelectAll(widgetVar,operate,msg,needConfirm)
{
	if(!widgetVar.isCheckAny())
	{
		alert("请选择要"+operate+"的"+msg);
		return false;
	} 
	else if(needConfirm)
	{
		if(!confirm("确定"+operate+"?")) 
		{
			return false;	
		}
	}
	return true;
}

// 判断是否选中
function isSelectWithExcel(msg,excelExport)
{
	if(excelExport)
	{
		if(jQuery("input:checked").length==0||(jQuery("input:checked").length==1&&jQuery("input[id$=excel_pageOnly]").attr("checked")==true))
		{
			alert("请选择要"+msg);
			return false;
		}
		return true;
	}
	else
	{
		if(jQuery("input:checked").length==0)
		{
			alert("请选择要"+msg);
			return false;
		}
		return true;
	}
}

function selectall(value)
	 { 
       var checkboxes = document.getElementsByTagName("input");
       for (i = 0; i < checkboxes.length; i++)
       { 
           if(checkboxes[i].type=='checkbox'&&checkboxes[i].id!='ctl00_ContentPlaceHolder1_selectDate')
           {
                if (value.checked)
                 { 
                    checkboxes[i].checked = true; // this checks all the boxes
                 }
                else
                 { 
                    checkboxes[i].checked = false; // this unchecks all the
													// boxes
                 } 
             }
       } 
     } 

// 自动显示：页面右侧的返回或关闭按钮
function Show_Back_Or_Close()
{
    if (window.opener==null)
      {
        document.all("spanClose").style.display="none";
        document.all("spanReturn").style.display="";
      }
      else
      {
         document.all("spanClose").style.display="";
         document.all("spanReturn").style.display="none";
      } 
}
 
 function MoveWindow(left,top)
{
  window.moveTo(left>0?left:0,top>0?top:0);
}

// 点击删除按钮，没有选择记录，要做判断
 function deletePrompt(name)
 {
	  if(jQuery("input:checked").length==0)
	  {
		  alert('请选择要删除的'+name);
		  return false;
	  }
	  if(!confirm('确定要删除选定的'+name+'吗?')) 
		  return false;
	  else
		  return true;
	  
 }
 
 // ========================
// 刷新父窗口页面
// ========================
function refresh(){
	window.location=window.location.href.replace("#","");
}


function refreshParent() 
{ 
	try
	{
		var temp=top.window.opener.location.href; 
		if(temp.indexOf("#") != -1)
			temp=temp.substring(0,temp.length-1);
// alert(temp);
		if(temp.indexOf("maingeneral.aspx")<0 && temp.indexOf("outlookmenu.aspx")<0 && temp.indexOf("waithandle/waithandle_done.aspx")<0 && temp.indexOf("waithandle/waithandle_del.aspx")<0)
		{
			
			top.window.opener.location.href=temp; 
		}
	}
	catch(err)
	{top.window.close();}
}

function RefreshParentPage() 
{ 
	try
	{
		// yb注释 2009-10-29
		// var temp=parent.window.opener.location.href.toLowerCase();
		var temp=parent.window.opener.location.href;
		if(temp.indexOf("#") != -1)
			temp=temp.substring(0,temp.length-1);
		if(temp.indexOf("maingeneral.aspx")<0 && temp.indexOf("outlookmenu.aspx")<0 && temp.indexOf("waithandle/waithandle_done.aspx")<0 && temp.indexOf("waithandle/waithandle_del.aspx")<0)
		{
			parent.window.opener.location.href=temp; 
		}
		parent.window.close();
	}
	catch(err)
	{parent.window.close();}
}

 // ========================
// 刷新新窗口的父框架页面
// ========================

function refreshOpenerParent() 
{ 
	try
	{
		var temp=top.window.opener.parent.location.href; 
		if(temp.indexOf("#") != -1)
			temp=temp.substring(0,temp.length-1);
		if(top.window.opener.parent.location.href.indexOf("mainGeneral.aspx")<=0)
		{
			top.window.opener.parent.location.href=temp; 
		}
	}
	catch(err)
	{top.window.close();}
}

// ========================
// 选择多级代码
// ========================
function CodeLevSelect(txt,Url)
	{
		var strFeatures="dialogWidth=400px;dialogHeight=600px;help=no;status=no";
		var temp=window.showModalDialog(Url,'',strFeatures);
		if(temp!=null&&temp!='')
		{
			txt.value=temp;
		}		
	}

// ========================
// 弹出一个对话框，选择后返回值，赋给文本框
// ========================
function SelectUrl(txt,Url)	
{	
	var strFeatures="dialogWidth=500px;dialogHeight=300px;help=no;status=no";
	var temp=window.showModalDialog(Url,'',strFeatures);
	if(temp!=null&&temp!='')
	{
		txt.value=temp;
	}
}


// ========================
// 窗口大小调整
// ========================
 
function ResizeWindow(width,height)
{
  if (width==screen.width)
    width=screen.availWidth;
  if (height==screen.height)
    height=screen.availHeight;
  var left=(screen.availWidth-width)/2;
  var top=(screen.availHeight-height)/2;
  window.moveTo(left>0?left:0,top>0?top:0);
  window.resizeTo(width,height);
}

// ========================
// 打开窗口
// ========================
function OpenWindow(url,width,height)
{
  if (width==null)
    width=788;
  else
    width=width-12;
  if (height==null)
    height=569;
  else
    height=height-31;
    
   LeftPosition = (screen.width) ? (screen.width-width)/2 : 0;
   TopPosition = (screen.height) ? (screen.height-height)/2 : 0;
   
   
   window.open(url,'_blank','channelmode=no,directories=no,fullscreen=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=yes,titlebar=yes,toolbar=no,left='+LeftPosition+',top='+ TopPosition +',width='+width+',height='+height);

   // window.open(url,'_blank','channelmode=no,directories=no,fullscreen=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=yes,titlebar=yes,toolbar=no,left='+(screen.availWidth<'+width+'?0:(screen.availWidth-'+width+')/2)+',top='+(screen.availHeight<'+height+'?0:(screen.availHeight-'+height+')/2)+',width='+width+',height='+height);
}

function OpenWindowNoResizable(url,width,height)
{
  if (width==null)
    width=788;
  else
    width=width-12;
  if (height==null)
    height=569;
  else
    height=height-31;
    
   LeftPosition = (screen.width) ? (screen.width-width)/2 : 0;
   TopPosition = (screen.height) ? (screen.height-height)/2 : 0;
   
   
   window.open(url,'_blank','channelmode=no,directories=no,fullscreen=no,location=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=yes,toolbar=no,left='+LeftPosition+',top='+ TopPosition +',width='+width+',height='+height);

   // window.open(url,'_blank','channelmode=no,directories=no,fullscreen=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=yes,titlebar=yes,toolbar=no,left='+(screen.availWidth<'+width+'?0:(screen.availWidth-'+width+')/2)+',top='+(screen.availHeight<'+height+'?0:(screen.availHeight-'+height+')/2)+',width='+width+',height='+height);
}

// ========================
// 打开最大化的窗口
// ========================
function OpenMaxWindow(url)
{
  window.open(url,'_blank','channelmode=no,directories=no,fullscreen=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=yes,titlebar=yes,toolbar=no,left='+(screen.availWidth<'+width+'?0:(screen.availWidth-'+width+')/2)+',top='+(screen.availHeight<'+height+'?0:(screen.availHeight-'+height+')/2)+',width='+screen.availWidth+',height='+screen.availHeight);
}

function OpenWindow(url,width,height,left,top)
{
  if (width==null)
    width=788;
  else
    width=width-12;
  if (height==null)
    height=569;
  else
    height=height-31;
 
 // add by liq 2008-9-22 ?
   left = (screen.width) ? (screen.width-width)/2 : 0;
  top = (screen.height) ? (screen.height-height)/2 : 0;
  window.open(url,'_blank','channelmode=no,directories=no,fullscreen=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=yes,titlebar=yes,toolbar=no,left='+left+',top='+top+',width='+width+',height='+height);
}


// ========================
// 打开窗口
// ========================
function OpenPrintWindow(url)
{ 
  window.open(url,'','width=1,height=1,top=10000,left=10000');
  // window.open("about:blank","","width=1,height=1,top=10000,left=10000");
}

// ========================
// 打开对话框
// ========================

function OpenDialog(url,width,height)
{
  today=new Date();  
  if (url.indexOf("?")<0)
  {
     url=url+"?xxxx="+Math.random();
  }
  else
  {
     url=url+"&xxxx="+Math.random();
  }
  if (width==null)
    width=800;
  if (height==null)
    height=600;
  return window.showModalDialog(url,'','dialogHeight:'+height+'px;dialogWidth:'+width+'px;edge:raised;center:Yes;help:No;resizable:Yes;status:yes;scroll:yes;unadorned:yes;');
}
// ========================
// 打开对话框
// ========================

function OpenDialogArgs(url,args,width,height)
{   
  today=new Date();  
  if (url.indexOf("?")<0)
  {
     url=url+"?xxxx="+Math.random();
  }
  else
  {
     url=url+"&xxxx="+Math.random();
  }
  if (width==null)
    width=800;
  if (height==null)
    height=600;
  return window.showModalDialog(url,args,'dialogHeight:'+height+'px;dialogWidth:'+width+'px;edge:raised;center:Yes;help:No;resizable:Yes;status:no;scroll:yes;unadorned:yes;');
}

// ========================
// 打开对话框带IFrame,并刷新父窗体
// 结合rtnValue使用 返回值为空 刷新父窗体
// 请勿修改 lsx ,若要用返回值的函数 请使用OpenDialog or OpenDialogArgs
// ========================

function OpenDialogRefresh(url,args,width,height)
{ 
  today=new Date(); 
  if (url.indexOf("?")<0)
  {
     url=url+"?xxxx="+Math.random();  
  }
  else
  {
     url=url+"&xxxx="+Math.random();
  }
  if (width==null)
    width=600;
  if (height==null)
    height=500;
  var rtnValue=window.showModalDialog(url,args,'dialogHeight:'+height+'px;dialogWidth:'+width+'px;resizable:Yes;scroll:Yse;status:Yes;');
    
  if(rtnValue!=null)
    {
        window.location=window.location.href.replace("#","");
    }  
}

// ========================
// 打开对话框,在关闭对话框的时候,并刷新父窗体，不管对话框是否有返回值
// ========================
function OpenDialogRefreshParent(url,args,width,height)
{ 
  today=new Date();  
  if (url.indexOf("?")<0)
  {
     url=url+"?xxxx="+Math.random();
  }
  else
  {
     url=url+"&xxxx="+Math.random();
  }
  if (width==null)
    width=600;
  if (height==null)
    height=500;
  var rtnValue=window.showModalDialog(url,args,'dialogHeight:'+height+'px;dialogWidth:'+width+'px;edge:raised;center:Yes;help:No;resizable:Yes;status:no;scroll:yes;unadorned:yes;');
        window.location=window.location.href.replace("#","");
}

// ========================
// 带IFrame的对话框返回值
// 结合OpenDialogRefresh使用
// ========================

function rtnValue(value)
{ 
  
    var win;
    if (window.parent==null)
    {
       win=window;
    }
    else
    {
       win=window.parent;
    }
    if(value=="")
    {
         win.returnValue="";	    
		 win.close();	
    }
    else if(value=="★")
    {
         win.returnValue="";
    } 
    else
    {
        alert(value);
    }
}

// ========================
// 带IFrame的对话框返回值
// ========================

function rtnValueNoIFrame(value)
{ 
    if(value=="")
    {
         window.returnValue="";	    
		 window.close();	
    }else if(value=="★")
    {
         window.returnValue="";
    }else
    {
        alert(value);
    }
}

// ========================
// 变换datagrid中行的背景色
// ========================
function swapon()
{
	if (this.className=="blueoff"){
		this.className="blueon";
		}
}

function swapoff()
{
	if (this.className=="blueon"){
		this.className="blueoff";
		}
}

// ========================
// 全选
// ========================
function AllSelect(parentCheckBox)
{
	var participants = document.getElementsByTagName("input");
    // alert(parentCheckBox.name);
	for ( var i=0; i < participants.length; i++ ) 
	{
		if(participants[i].name.indexOf(parentCheckBox.name)!=-1 && !participants[i].disabled)
		{				
			var participant = participants[i];
			if ( participant != null ) {
				participant.checked = parentCheckBox.checked;
			}
		}
	}
}


// 检查输入的键盘字符是否合法，只允许输入数字字符
function CheckInputKey()
{
	if (event.keyCode==59)		   
	event.returnValue = false;
	else
	{		      
			if ((event.keyCode >47 && event.keyCode <58)||event.keyCode==8|| 
			event.keyCode==46 || event.keyCode==37 || event.keyCode==39 ||event.keyCode==190) 
			{
				event.returnValue = true;
			}
			else event.returnValue = false;	
	}			
}	

// 窗口最大化
function MaxWindow()
{
    try
    {
		window.moveTo(0,0);
		window.resizeTo(window.screen.availWidth,window.screen.availHeight);
    }
	catch(err)
	{}
   	
}

// ========================
// 全选、全不选
// ========================
function SelectCheckbox(PrefixName,blnSelect)
{
	var participants = document.getElementsByTagName("input");

	for ( var i=0; i < participants.length; i++ ) 
	{
		if(participants[i].name.indexOf(PrefixName)!=-1)
		{				
			var participant = participants[i];
			if ( participant != null ) {
				participant.checked = blnSelect;
			}
		}
	}
}

// ========================
// 检查Grid中的第一列选择框是否选中
// ========================
function Check_SelectedStatus(PrefixName,Message)
{
    if (PrefixName==null)
         PrefixName="chkAdd";
         
    var participants = document.getElementsByTagName("input");

	for ( var i=0; i < participants.length; i++ ) 
	{
		if(participants[i].name.indexOf(PrefixName)!=-1)
		{				
			var participant = participants[i];
			if ( participant != null ) {
			    if (participant.checked)
			    {
			        return true;
			    }				
			}
		}
	}
	if(Message==null)
	{
	   alert("请选择记录!");
	}
	else
	{
	   alert(Message);
	}	
	return false;

}

// 检查允许上传的文件类型
function CheckFile(FileLst)
{
  // 取得允许上传或不允许上传的文件属性
  FileLst = FileLst.toLowerCase();
  if(FileLst.substring(0,1)!=";")
      FileLst = ";" + FileLst;
  
  if(FileLst.substring(FileLst.length-1)!=";")
   FileLst =  FileLst + ";" ;
  
  var inputs = document.getElementsByTagName("INPUT");
  var inputFile;
  
  var uname;
  var ftype;
  for(var i=0;i<inputs.length;i++)
  {
	    inputFile = inputs[i];
		if(inputFile.type == "file")
		{
				uname = inputFile.value;
				if(uname != "")
				{
					ftype = uname.substring(uname.lastIndexOf(".")+1).toLowerCase();    
					if(FileLst.indexOf(";" + ftype + ";") < 0)
					{
						alert("您选择的文件:" + inputFile.value + " 不在系统允许上传的文件类型中!\r\n系统允许上传的文件格式有:" + FileLst);
						return false;
					}// IF 不在列表中
				}// IF 名称非空
			
    	}// IF FILE
    }// FOR
    return true; 
}// END

// ===============================================
// 检查上传的文件的类型和大小，如果不在允许范围内或超过最大值，就返回False
// ===============================================
function CheckUploadFile(FileLst,maxsize)
{
    if (CheckFile(FileLst))
    {
        // try
		// / {
		// return chkUploadFileSize(maxsize);
		// }
		// catch(err)
		// {
		// alert("脚本错误："+err.message+"!\r\n\r\n请检查：\r\n1/可信任站点是否设置？\r\n2/ActiveX控件和插件相关属性是否启用？");
		// return false;
		// }
		    
        try   // 这里一定要有try /catch
        {
            return chkUploadFileSize(maxsize);
        }
        catch(err)
        {           
            return true;     
        }
    }
    else
       return false;
}
// ===============================================
// 检查上传的文件大小，如果超过最大值，就返回False
// ===============================================
function chkUploadFileSize(maxsize)
{
	var fso = new ActiveXObject('Scripting.FileSystemObject');
	// var spanId="idfilespan";
	// var span = document.getElementById(spanId);
	var inputFile;
	var File;
	if(document.getElementsByTagName("INPUT").length==1)
		return true;
	else
	{
		
		if(document.getElementsByTagName("INPUT").length>1)
		{
			
			for(var i=0;i<document.getElementsByTagName("INPUT").length;i++)
			{
				inputFile = document.getElementsByTagName("INPUT")[i];
				if(inputFile.type!="file")  continue;
				// alert(inputFile.value);
				if(!fso.FileExists(inputFile.value)) continue;
				File = fso.GetFile(inputFile.value);
				// alert(File.Size);
				if(File.Size>maxsize*1024*1024)
				{
					
					alert ("抱歉！您选择的上传文件超过了" + maxsize +"M，不能提交！");
					return false;
				}
			}
		}
	}
	return true;
}

// ========================
// 日期变量上加上指定时间间隔
// strInterval：s-秒；n-分；h-小时；d-天；w-周；m-月
// ========================
function DateAdd(strInterval, NumDay, dtDate)
 {
	var dtTmp = new Date(dtDate);
	if (isNaN(dtTmp)) dtTmp = new Date();
	switch (strInterval) {
	case "s":return new Date(Date.parse(dtTmp) + (1000 * NumDay));
	case "n":return new Date(Date.parse(dtTmp) + (60000 * NumDay));
	case "h":return new Date(Date.parse(dtTmp) + (3600000 * NumDay));
	case "d":return new Date(Date.parse(dtTmp) + (86400000 * NumDay));
	case "w":return new Date(Date.parse(dtTmp) + ((86400000 * 7) * NumDay));
	case "m":return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + NumDay, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
	case "y":return new Date((dtTmp.getFullYear() + NumDay), dtTmp.getMonth(), dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
	}
}

// ========================
// 日期变量之间的间隔
// strInterval：s-秒；n-分；h-小时；d-天；w-周；m-月
// ========================
// DateDiff("m","2002/12/1 00:00:00","2004/12/1 00:00:00")
function DateDiff(strInterval, dtStart, dtEnd)
{
	var dtStart = new Date(dtStart);
	if (isNaN(dtStart)) dtStart = new Date();
	var dtEnd = new Date(dtEnd);
	if (isNaN(dtEnd)) dtEnd = new Date();
	switch (strInterval)
	{
		case "s":return parseInt((dtEnd - dtStart) / 1000);
		case "n":return parseInt((dtEnd - dtStart) / 60000);
		case "h":return parseInt((dtEnd - dtStart) / 3600000);
		case "d":return parseInt((dtEnd - dtStart) / 86400000);
		case "w":return parseInt((dtEnd - dtStart) / (86400000 * 7));
		case "m":return (dtEnd.getMonth()+1)+((dtEnd.getFullYear()-dtStart.getFullYear())*12) - (dtStart.getMonth()+1);
		case "y":return dtEnd.getFullYear() - dtStart.getFullYear();
	}
}


// /read cookie function


function GetCookie (name)
{
    // alert("")
	var arg = name + "=";
	var alen = arg.length;
	var clen = document.cookie.length;
	var i = 0;
	while (i < clen)
	{
		var j = i + alen;
		if (document.cookie.substring(i, j) == arg)
			return getCookieVal (j);
		i = document.cookie.indexOf(" ", i) + 1;
		if (i == 0)	
			break;
	}
	return null;
}

function getCookieVal (offset)
{
var endstr = document.cookie.indexOf (";", offset);
if (endstr == -1)
endstr = document.cookie.length;
return unescape(document.cookie.substring(offset, endstr));
}
// /read cookie function



		
    // 在指定的输入控件中插入文本
	function   InsertTextAtFocusPostion(InputControl,InsertText)
	{
		InputControl.focus();				
		document.selection.createRange().text+=InsertText;
	}
	
	
// 2007-11-17 ADD BY LIQ
// 客户端生成一个Guid
function GetGuid()
{
    var   guid   =   "";
	for   (var   i   =   1;   i   <=   32;   i++)
	  {
	  var   n   =   Math.floor(Math.random()   *   16.0).toString(16);
	  guid   +=   n;
	  if   ((i   ==   8)   ||   (i   ==   12)   ||   (i   ==   16)   ||   (i   ==   20))
		guid   +=   "-";
	  }
	return guid;
}


// 根据头部框架页内容，自动调整iframe高度
// 用于工作流处理页面中
function dyniHeaderHeight(headerFrameId)
{  
    var getFFVersion=navigator.userAgent.substring(navigator.userAgent.indexOf("Firefox")).split("/")[1]   
    var FFextraHeight=getFFVersion>=0.1? 16 : 0   
    var pTar = null;   
    if (document.getElementById){   
       pTar = document.getElementById(headerFrameId);   // 'header'
    }
    if (pTar && !window.opera){   
       pTar.style.display="block"  
       if (pTar.contentDocument && pTar.contentDocument.body.offsetHeight){  
           pTar.height = pTar.contentDocument.body.offsetHeight+FFextraHeight;    
       } else if (pTar.Document && pTar.Document.body.scrollHeight){ 
            pTar.height = pTar.Document.body.scrollHeight;   
       }   
    }   
}   



/*
 * ============================================== //以下的方法用于Div 消息窗口的显示
 * ==============================================
 */
var EP_MessageDivID = "EPMessageDiv";
var EP_MessageDiv;
var EP_MessageHasShow = false;

var EP_MessageIFrame;
if(typeof(EP_IE) != "boolean")
	var EP_IE = (document.all) ? true : false;

function EP_getElementById(id)
{
    
	var el;
	if(EP_IE) 
		el = document.all[id];
	else 
		el = document.getElementById(id);
	return el;
}

// obj 控件对象 ，Msg 提示信息，Type,1 使用alert，否则使用弹出Div
function EP_ShowMessageDiv(obj,Msg,type)
{
    if(type=='1')
    {
        alert(Msg);
        obj.Element.focus;
        obj.Element.select();
        return;
    }
    
    if(typeof(EP_getElementById("IFrame" + EP_MessageDivID))=="undefined")
    {
        EP_MessageIFrame = document.createElement("iframe");
        EP_MessageIFrame.id = "IFrame" + EP_MessageDivID;
        EP_MessageIFrame.scrolling = "no";
        EP_MessageIFrame.frameborder = "no";
        EP_MessageIFrame.style.position = "absolute";
        EP_MessageIFrame.style.top = "0px";
        EP_MessageIFrame.style.left = "0px";
        EP_MessageIFrame.style.display = "none";
        EP_MessageIFrame.style.filter = "Alpha(Opacity=100)";
        window.document.body.insertAdjacentElement("afterBegin", EP_MessageIFrame)

    }
	if(typeof(EP_getElementById(EP_MessageDivID))=="undefined")
    {
        EP_MessageDiv=document.createElement("div");
        EP_MessageDiv.id=EP_MessageDivID;
        EP_MessageDiv.style.color = "#000";
        EP_MessageDiv.style.paddingBottom = "4px";
        EP_MessageDiv.style.position = "absolute";
        EP_MessageDiv.style.zIndex = 10;
        EP_MessageDiv.style.fontSize = "9pt";
        EP_MessageDiv.style.background = "url(" + Epoint_BackgroundImage + ") no-repeat left bottom";
        window.document.body.insertAdjacentElement("afterBegin", EP_MessageDiv)
    }
    
    
    EP_MessageDiv.innerHTML = "";
    
    
    if(EP_MessageHasShow)
    {
        EP_MessageDiv.filters.Alpha.opacity = 100;
        EP_MessageIFrame.filters.Alpha.opacity = 100;
        EP_MessageDiv.insertAdjacentHTML("beforeEnd", "<div style=\"background:#ffc;border-left:1px solid #000; border-right:1px solid #000; border-top:1px solid #000; padding:2px 3px 0;white-space:nowrap;\"><img src='" + Epoint_ErrorImage + "'/> " + Msg.ReplaceAll("\n","<br>") + "</div>");
        
        var pos = EPGetAbsoluteLocation1(obj);
	    EP_MessageDiv.style.top   =   pos.absoluteTop - EP_MessageDiv.offsetHeight-2;
	    EP_MessageDiv.style.left   =   pos.absoluteLeft;        
        
        pos = EPGetAbsoluteLocation1(EP_getElementById(EP_MessageDivID));
        EP_MessageIFrame.style.width   =   pos.offsetWidth;
        EP_MessageIFrame.style.height   =   parseInt(pos.offsetHeight) - 2;
        EP_MessageIFrame.style.top   =   EP_MessageDiv.style.top;
        EP_MessageIFrame.style.left   =   EP_MessageDiv.style.left;
        EP_MessageIFrame.style.zIndex   =   EP_MessageDiv.style.zIndex   -   1;
        EP_MessageIFrame.style.display = "";
    }
    else
    {    
      
   
        EP_MessageHasShow = true;
        EP_MessageDiv.style.display = "";
        EP_MessageDiv.style.filter = "Alpha(Opacity=100)";
        EP_MessageIFrame.style.filter = "Alpha(Opacity=100)";

        EP_MessageDiv.insertAdjacentHTML("beforeEnd", "<div style=\"background:#ffc;border-left:1px solid #000; border-right:1px solid #000; border-top:1px solid #000; padding:2px 3px 0;white-space:nowrap;\"><img src='" + Epoint_ErrorImage + "'/> " + Msg.ReplaceAll("\n","<br>") + "</div>");
        var pos = EPGetAbsoluteLocation1(obj);
	    EP_MessageDiv.style.top   =   pos.absoluteTop - EP_MessageDiv.offsetHeight-2;
	    EP_MessageDiv.style.left   =   pos.absoluteLeft;
        pos = EPGetAbsoluteLocation1(EP_getElementById(EP_MessageDivID));
        
        EP_MessageIFrame.style.width   =   pos.offsetWidth;
        EP_MessageIFrame.style.height   =   parseInt(pos.offsetHeight) - 2;
        EP_MessageIFrame.style.top   =   EP_MessageDiv.style.top;
        EP_MessageIFrame.style.left   =   EP_MessageDiv.style.left;
        EP_MessageIFrame.style.zIndex   =   EP_MessageDiv.style.zIndex   -   1;
        EP_MessageIFrame.style.display = "";
        EP_RemoveMessageDiv();
    }

}


// 渐渐隐藏错误信息提示
function EP_RemoveMessageDiv()
{
    if(EP_MessageHasShow && EP_MessageDiv.style.display == "")
    {
        if(EP_MessageDiv.filters.Alpha.opacity>1)
        {
            EP_MessageDiv.filters.Alpha.opacity--;
            EP_MessageIFrame.filters.Alpha.opacity--;
            window.setTimeout("EP_RemoveMessageDiv()",5);
        }
        else
        {
            EP_MessageDiv.style.display = "none";
            EP_MessageDiv.style.filter = "";
            EP_MessageHasShow = false;
            EP_MessageIFrame.style.display = "none";
        }
    }
}

var w3c=(document.getElementById)? true:false;
var agt=navigator.userAgent.toLowerCase();
var ie = ((agt.indexOf("msie") != -1) && (agt.indexOf("opera") == -1) && (agt.indexOf("omniweb") == -1));
var ie5=(w3c && ie)? true : false;
var ns6=(w3c && (navigator.appName=="Netscape"))? true: false;

function EPGetAbsoluteLocation1(o)
{
    var nLt=0;
    var nTp=0;
    var offsetParent = o;
    while (offsetParent!=null && offsetParent!=document.body) 
    {
        nLt+=offsetParent.offsetLeft;
        nTp+=offsetParent.offsetTop;
        if(!ns6)
        {
            parseInt(offsetParent.currentStyle.borderLeftWidth)>0?nLt+=parseInt(offsetParent.currentStyle.borderLeftWidth):"";
            parseInt(offsetParent.currentStyle.borderTopWidth)>0?nTp+=parseInt(offsetParent.currentStyle.borderTopWidth):"";
        }
        offsetParent=offsetParent.offsetParent;
    }
    return { absoluteTop: nTp, absoluteLeft: nLt, 
        offsetWidth: o.offsetWidth, offsetHeight: o.offsetHeight }; 
        
       
}



function OpenChat(ToUserGuid,MyUserGuid,OpenUrl)
{	  
    if(ToUserGuid==MyUserGuid)
    {
        alert("不能跟自己在线交流！");
    }
    else
    {
        var url=OpenUrl; 
        window.open(url,'','Width=609,Height=445,top=0,left=0,help:no,status:no,scroll=no,resizable=no');
    }
}


/**
 * 模态窗口高度调整. 根据操作系统及ie不同版本,重新设置窗口高度,避免滚动条出现.
 */
function resetDialogHeight(){
      if(window.dialogArguments == null){
        return; // 忽略非模态窗口
      }

      var ua = navigator.userAgent;
      var height = document.body.offsetHeight;
      if(ua.lastIndexOf("MSIE 6.0") != -1){
      if(ua.lastIndexOf("Windows NT 5.1") != -1){
        // alert("xp.ie6.0");
        var height = document.body.offsetHeight;
        window.dialogHeight=(height+102)+"px";
      }
      else if(ua.lastIndexOf("Windows NT 5.0") != -1){
        // alert("w2k.ie6.0");
        var height = document.body.offsetHeight;
        window.dialogHeight=(height+49)+"px";
      }
    }
} 

function openChatWindow(UserGuid,myUserGuid,url)
{
	if(myUserGuid!=""&&UserGuid==myUserGuid)
    {
        alert("不能跟自己在线交流！");
    }
    else
    {
        window.open(url,'','width=609,height=520,top=0,left=0,help=no,scroll=no,resizable=no');
    }
}

// =========================
// Div Alert
// =======================
function sAlert(str){
        var msgw,msgh,bordercolor;
        msgw=400;
        msgh=100;
        bordercolor="#336699";
        titlecolor="#99CCFF";

        var sWidth,sHeight;
        sWidth=document.body.offsetWidth;
        sHeight=document.body.offsetHeight;

        var bgObj=document.createElement("div");
        bgObj.setAttribute('id','bgDiv');
        bgObj.style.position="absolute";
        bgObj.style.top="0";
        bgObj.style.background="#777";
        bgObj.style.filter="progid:DXImageTransform.Microsoft.Alpha(style=3,opacity=25,finishOpacity=75";
        bgObj.style.opacity="0.6";
        bgObj.style.left="0";
        bgObj.style.width=sWidth + "px";
        bgObj.style.height=sHeight + "px";
        document.body.appendChild(bgObj);
        var msgObj=document.createElement("div")
        msgObj.setAttribute("id","msgDiv");
        msgObj.setAttribute("align","center");
        msgObj.style.position="absolute";
        msgObj.style.background="white";
        msgObj.style.font="12px/1.6em Verdana, Geneva, Arial, Helvetica, sans-serif";
        msgObj.style.border="1px solid " + bordercolor;
        msgObj.style.width=msgw + "px";
        msgObj.style.height=msgh + "px";
        msgObj.style.top=(document.documentElement.scrollTop + (sHeight-msgh)/2) + "px";
        msgObj.style.left=(sWidth-msgw)/2 + "px";
        var title=document.createElement("h4");
        title.setAttribute("id","msgTitle");
        title.setAttribute("align","right");
        title.style.margin="0";
        title.style.padding="3px";
        title.style.background=bordercolor;
        title.style.filter="progid:DXImageTransform.Microsoft.Alpha(startX=20, startY=20, finishX=100, finishY=100,style=1,opacity=75,finishOpacity=100);";
        title.style.opacity="0.75";
        title.style.border="1px solid " + bordercolor;
        title.style.height="18px";
        title.style.font="12px Verdana, Geneva, Arial, Helvetica, sans-serif";
        title.style.color="white";
        title.style.cursor="pointer";
        title.innerHTML="?";
        title.onclick=function(){
            document.body.removeChild(bgObj);
            document.getElementById("msgDiv").removeChild(title);
            document.body.removeChild(msgObj);
        }
        document.body.appendChild(msgObj);
        document.getElementById("msgDiv").appendChild(title);
        var txt=document.createElement("p");
        txt.style.margin="1em 0"
        txt.setAttribute("id","msgTxt");
        txt.innerHTML=str;
        document.getElementById("msgDiv").appendChild(txt);
 }
String.prototype.Trim = function() {return this.replace(/(^\s*)|(\s*$)/g,"");} 
