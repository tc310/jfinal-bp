package com.topteam.component.freemarker;

import com.alibaba.druid.sql.dialect.sqlserver.ast.Top;
import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;
import com.topteam.frame.controllor.BaseController;

public class FreeMarkerInterceptor implements Interceptor {

	@Override
	public void intercept(ActionInvocation ai) {
		Controller c = ai.getController();
		
		if(c instanceof BaseController){
			BaseController bc = (BaseController)c;
			bc.setAttr(TopButton.TAG, new TopButton(bc));
			bc.setAttr(TopSelect.TAG, new TopSelect(bc));
			bc.setAttr(TopUpload.TAG, new TopUpload(bc));
			bc.setAttr(TopBoolCheckbox.TAG, new TopBoolCheckbox(bc));
			bc.setAttr(TopIntCheckbox.TAG, new TopIntCheckbox(bc));
			bc.setAttr(TopDataTable.TAG, new TopDataTable(bc));
			bc.setAttr(TopColumns.TAG, new TopColumns(bc));
			bc.setAttr(TopColumn.TAG, new TopColumn(bc));
			bc.setAttr(TopLayout.TAG, new TopLayout(bc));
			bc.setAttr(TopLayoutUnit.TAG, new TopLayoutUnit(bc));
			bc.setAttr(TopCalendar.TAG, new TopCalendar(bc));
			bc.setAttr(TopCheckBoxGroup.TAG, new TopCheckBoxGroup(bc));
			bc.setAttr(TopAuth.TAG, new TopAuth(bc));
		}
		
		ai.invoke();

	}

}
