package com.topteam.component.freemarker;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.impl.conn.Wire;

import com.topteam.frame.controllor.BaseController;
import com.topteam.utility.StringUtil;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class TopUpload extends TopTemplateModel{
	
	public static final String TAG = "upload";
	
	private boolean multi = false;
	
	private boolean auto = false;
	
	private String id;
	private String btnText;
	
	private String url;
	
	private String targetId;
	
	private String onComplete;
	
	private String fileTypes;
	
	private String tag;
	
	
	private HttpServletRequest request;
	private String root;
	public TopUpload(BaseController controller){
		this.request = controller.getRequest();
		root = request.getContextPath();
	}

	@Override
	public void execute(Environment env, Map para, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		initPara(para,env);
		
		Writer w = env.getOut();
		w.write("<input type=\"file\" name=\""+id+"\" id=\""+id+"\" />");
		if(!auto){
			w.write("<div id=\""+id+"-upload-btns\">");
			w.write(" <a class=\"easyui-linkbutton\" href=\"javascript:$('#"+id+"').uploadify('upload', '*');\">开始上传</a> ");
			if(multi){
				w.write("<a class=\"easyui-linkbutton\" href=\"javascript:$('#"+id+"').uploadify('cancel', '*');\">清除队列</a>");
			}
			w.write("</div>");
		}
		writeScript(w);
	}
	
	private void initPara(Map para, Environment env) throws TemplateException{
		multi = getBoolPara("multi", para, env,multi);
		id = getStrPara("id", para, env);
		btnText = getStrPara("btnText", para, env);
		auto = getBoolPara("auto", para, env,auto);
		url = getStrPara("url", para, env);
		targetId = getStrPara("targetId", para, env);
		
		if(StringUtil.isNull(url)){
			if(multi){
				url = root+"/upload/up";
			}else{
				url = root+"/upload/up";
			}
		}
		
		onComplete = getStrPara("onComplete", para, env);
		fileTypes = getStrPara("fileTypes", para, env);
		tag = getStrPara("tag", para, env);
	}
	
	private void writeScript(Writer w) throws IOException{
		w.write("<script type=\"text/javascript\">");
		w.write("	$(function() {");
		w.write("		$('#"+id+"').uploadify({");
		w.write("			'swf'      : '"+root+"/weblib/uploadify/uploadify.swf',");
		w.write("			'uploader' : '"+url+"',");
		w.write("			'multi': "+multi+",");
		w.write("			'auto' :"+auto+",");
		w.write("			'formData' :{targetId:'"+targetId+"',tag:'"+tag+"'},");
		w.write("			'buttonText':'"+(btnText==null?"选择附件":btnText)+"',");
		w.write("			'height'        : 22, ");
		w.write("			'width'         : 280,");
		if(!auto){
			w.write("			'myBtnsId'      :'"+id+"-upload-btns',");
		}
		if(StringUtil.isNotNull(onComplete)){
			w.write("			'onUploadSuccess'      :"+onComplete+",");
		}
		if(StringUtil.isNotNull(fileTypes)){
			w.write("			'fileTypeExts'      :'"+fileTypes+"',");
		}
		w.write("			'cancelImg': '"+root+"/weblib/uploadify/uploadify-cancel.png'");
		w.write("	});");
		w.write("});");
		w.write("</script>");
	}

}
