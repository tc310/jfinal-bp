package com.topteam.component.freemarker;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import com.topteam.frame.controllor.BaseController;
import com.topteam.utility.StringUtil;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * 分页表格控件
 * 
 * @author Jiang Feng
 * 
 */
public class TopDataTable extends TopTemplateModel {

	public static final String TAG = "datatable";

	private BaseController controller;

	private String id;

	private String title;

	private String model;

	private String toolbar;

	private boolean fit;

	private String style;
	
	private String groupName;
	
	private String groupFormatter;

	private boolean hideRowNum;

	private boolean hidePagination;

	private boolean hideCheckbox;
	private boolean multiple;
	
	private String onLoadSuccess;
	
	private String onClickRow;
	

	public TopDataTable(BaseController controller) {
		this.controller = controller;
	}

	private void initPara(Map para, Environment env) throws TemplateException {
		id = getStrPara("id", para, env);
		if (StringUtil.isNull(id)) {
			throw new TemplateException("必须直接按钮id的值", env);
		}
		model = getStrPara("model", para, env);
		if (StringUtil.isNull(model)) {
			throw new TemplateException("DataTable控件必须指定model", env);
		}
		title = getStrPara("title", para, env);
		fit = getBoolPara("fit", para, env,fit);
		style = getStrPara("style", para, env);
		toolbar= getStrPara("toolbar", para, env);
		groupName= getStrPara("groupName", para, env);
		groupFormatter= getStrPara("groupFormatter", para, env);
		hideRowNum = getBoolPara("hideRowNum", para, env,hideRowNum);
		hidePagination = getBoolPara("hidePagination", para, env,hidePagination);
		hideCheckbox = getBoolPara("hideCheckbox", para, env,hideCheckbox);
		multiple = getBoolPara("multiple", para, env,multiple);
		
		onLoadSuccess = getStrPara("onLoadSuccess", para, env);
		onClickRow = getStrPara("onClickRow", para, env);
		
	}

	@Override
	public void execute(Environment env, Map para, TemplateModel[] arg2,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		initPara(para, env);
		Writer w = env.getOut();
		w.write("<table id=\"" + id + "\" ");
		if (StringUtil.isNotNull(style))
			w.write("style=\"" + style + "\"");
		if(StringUtil.isNotNull(toolbar))
			w.write(" data-options=\"toolbar:'"+toolbar+"'\"");
		w.write(" >\r\n");
		body.render(w);
		w.write("</table>");
		writeScript(w);
	}

	public void writeScript(Writer w) throws IOException {
		w.write("<script type=\"text/javascript\">");
		w.write("$(function(){\r\n");
		w.write("	$('#" + id + "').datagrid({ \r\n");
		if (fit)
			w.write("		 fit:" + fit + ",\r\n");
		w.write("		 fitColumns: true,\r\n");
		w.write("		 pageSize : "+controller.getAttr("tableSize")+",\r\n");
		if (!hideRowNum) {
			w.write("		 rownumbers:true,\r\n");
		}
		if (!hidePagination) {
			w.write("		 pagination:true,\r\n");
		}
		if (!hideCheckbox) {
			w.write("		 checkbox:true,\r\n");
		}
		w.write("		 singleSelect:" + (!multiple) + ",\r\n");
		w.write("		 selectOnCheck:false,\r\n");
		if(StringUtil.isNotNull(groupName)){
			w.write("		 view:groupview,\r\n");
			w.write("		 groupField:'"+groupName+"',\r\n");
			if(StringUtil.isNotNull(groupFormatter)){
				w.write("		 groupFormatter:"+groupFormatter+",\r\n");
			}else{
				w.write("		 groupFormatter:function(value,rows){return value+' - '+rows.length+' 条记录'},\r\n");
			}
		}
		
		if(StringUtil.isNotNull(title))
			w.write("		title:'"+title+"',\r\n");
		
		if(StringUtil.isNotNull(onLoadSuccess))
			w.write("		 onLoadSuccess: "+onLoadSuccess+",\r\n");
		if(StringUtil.isNotNull(onClickRow)){
			w.write("		 onClickRow: "+onClickRow+",\r\n");
		}
		w.write("		 url:'" + model + "'\r\n");
		w.write("	});\r\n");
		w.write("});\r\n");
		w.write("</script>");
	}

}
