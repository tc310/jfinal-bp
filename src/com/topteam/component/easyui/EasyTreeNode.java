package com.topteam.component.easyui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EasyTreeNode {

	private String id;
	private String text;
	private String iconCls = "";
	private boolean checked;
	private String state;
	private List<EasyTreeNode> children;
	private Map<String,?> attributes;
	
	private boolean hasChild;
	
	public EasyTreeNode(){
		children = new ArrayList<EasyTreeNode>();
	}
	
	public void addChild(EasyTreeNode child){
		this.children.add(child);
	}

	public String getId() {
		return id==null? "":id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public String getState() {
		return hasChild?"closed":"opened";
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<EasyTreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<EasyTreeNode> children) {
		this.children = children;
	}

	public Map<String,?> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String,?> attributes) {
		this.attributes = attributes;
	}

	public boolean isHasChild() {
		return hasChild;
	}

	public void setHasChild(boolean hasChild) {
		this.hasChild = hasChild;
	}
	
}
