package com.topteam.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Properties;
import java.util.UUID;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.StringKit;

public class CommUtil {

	public static String md5(String s) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA1");
			md.update(s.getBytes());
			return bytes2Hex(md.digest()).toUpperCase();
		} catch (NoSuchAlgorithmException e) {
		}
		return "";
	}

	private static String bytes2Hex(byte[] bts) {
		StringBuilder des = new StringBuilder();
		String tmp = null;
		for (int i = 0; i < bts.length; i++) {
			tmp = Integer.toHexString(bts[i] & 0xFF);
			if (tmp.length() == 1) {
				des.append("0");
			}
			des.append(tmp);
		}
		return des.toString();
	}
	

}