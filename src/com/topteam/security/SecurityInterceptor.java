package com.topteam.security;

import java.lang.reflect.Method;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;
import com.jfinal.log.Logger;
import com.topteam.application.TopApplicationContext;

/**
 * 安全验证拦截器
 * @author 姜枫  2013-5-8
 *
 */
public class SecurityInterceptor implements Interceptor{
	
	public static Logger logger = Logger.getLogger(SecurityInterceptor.class);
	
	private TopApplicationContext applicationContext = TopApplicationContext.getInstance();

	public void intercept(ActionInvocation ai) {
		Controller controller = ai.getController();
		String actionKey = ai.getActionKey();
		
		Method method = ai.getMethod();
		Security security = method.getAnnotation(Security.class);
		if(security!=null && !security.depend().equals("")){
			actionKey = ai.getControllerKey()+security.depend();
		}
		
		//判断该请求是否需要安全验证
		if(!applicationContext.needLoginIntercept(actionKey)){
			ai.invoke();
			return;
		}
		
		UserSession session = controller.getSessionAttr("userSession");
		
		if(session!=null){
			if(applicationContext.needSecurithIntercept(actionKey)){
				if(!session.auth(actionKey)){
					controller.redirect("/error/404");
				}else{
					ai.invoke();
				}
			}else
				ai.invoke();
		}else{
			String callback = controller.getRequest().getServletPath();
			logger.debug(ai.getActionKey()+"无访问权限，重定向到登录页面");
			controller.setAttr("callback", callback);
			controller.redirect("/security/login");
		}
		
//		//判断用户是否登录，已经该用户是否有权限访问该请求
//		if(!applicationContext.needSecurithIntercept(actionKey) ){
//			ai.invoke();
//		}else if (session != null && session.auth(actionKey)){
//			ai.invoke();
//		}else{
//			
//			// 如果没有登录  或者  没有权限，获取回调页面，登录以后将会重新返回到这个页面
//			// 这个功能在 easyui 框架中，可能没有用，因为新页面都是在嵌套的iframe里面打开的，将来可以通过配置来是否开启
//			String callback = controller.getRequest().getServletPath();
//			logger.debug(ai.getActionKey()+"无访问权限，重定向到登录页面");
//			controller.setAttr("callback", callback);
//			controller.redirect("/security/login");
//		}
	}

}
