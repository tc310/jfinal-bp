package com.topteam.security;

import java.util.Comparator;

import com.topteam.frame.entity.FrameMenu;

class MenuComparator implements Comparator<FrameMenu>{

	@Override
	public int compare(FrameMenu o1, FrameMenu o2) {
		int order1 = o1.getInt("order");
		int order2 = o2.getInt("order");
		return order2-order1;
	}

}
