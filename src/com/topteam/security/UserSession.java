package com.topteam.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.topteam.frame.entity.FrameMenu;
import com.topteam.frame.entity.FrameRole;
import com.topteam.frame.entity.FrameUser;
import com.topteam.frame.entity.FrameUserExt;

/**
 * 用户相关信息 Session 对象
 * 
 * @author 姜枫 2013-5-8
 * 
 */
public class UserSession implements Serializable{

	private static final long serialVersionUID = -4582164094690393729L;

	private Map<String,FrameMenu> menuKey = new HashMap<String,FrameMenu>();
	private Map<String,FrameMenu> btnKey = new HashMap<String,FrameMenu>();
	private Set<String> menuKeySet = new HashSet<String>();
	private Set<String> btnKeySet = new HashSet<String>();
	private Set<String> roleSet = new HashSet<String>();
	
	private String userid;

	private String username;
	
	private String displayName;

	private String userTheme = "metro-blue";

	private String sysMsgPosition = "top-center";

	private int tableSize = 20;

	public UserSession(FrameUser u) {
		this.userid = u.getStr("id");
		this.username = u.getStr("username");
		this.displayName = u.getStr("displayname");
		FrameUserExt userExt = FrameUserExt.dao.getUserExtByUserId(u.getStr("id"));
		if(userExt!=null){
			this.userTheme = userExt.getStr("theme");
			this.sysMsgPosition = userExt.getStr("sysMsgPosition");
			this.tableSize = userExt.getInt("tableSize");
		}
		initUserAuth();
		initUserRole();
	}

	private void initUserAuth() {
		List<FrameMenu> menuList = FrameMenu.dao.queryAuth("MENU", userid);
		List<FrameMenu> btnList = FrameMenu.dao.queryAuth("BTN", userid);
		for(FrameMenu menu:menuList){
			menuKey.put(menu.getStr("id"), menu);
			menuKeySet.add(menu.getStr("menuUrl"));
		}
		for(FrameMenu menu:btnList){
			btnKey.put(menu.getStr("id"), menu);
			btnKeySet.add(menu.getStr("menuUrl"));
		}
	}
	
	private void initUserRole(){
		List<FrameRole> roleList = FrameRole.dao.queryRoleByUserId(userid);
		for(FrameRole role:roleList){
			roleSet.add(role.getStr("roleName"));
		}
	}
	
	public boolean auth(String actionKey) {
		return menuKeySet.contains(actionKey);
	}
	
	public boolean authBtn(String actionKey){
		return btnKeySet.contains(actionKey);
	}
	
	public List<FrameMenu> getRootMenu(){
		Collection<FrameMenu> collection = menuKey.values();
		List<FrameMenu> list = new ArrayList<FrameMenu>();
		for (FrameMenu menu:collection) {
			if(menu.getStr("menuCode").length()==4)
				list.add(menu);
		}
		Collections.sort(list, new MenuComparator());
		return list;
	}
	
	public List<FrameMenu> getMenusById(String id) {
		Collection<FrameMenu> collection = menuKey.values();
		List<FrameMenu> list = new ArrayList<FrameMenu>();
		for (FrameMenu menu:collection) {
			if(menu.getStr("parentId")!=null&&menu.getStr("parentId").equals(id))
				list.add(menu);
		}
		Collections.sort(list, new MenuComparator());
		return list;
	}

	public String getUsername() {
		return username;
	}

	public String getUserTheme() {
		return userTheme;
	}
	
	public String getUserid() {
		return userid;
	}

	public void setUserTheme(String userTheme) {
		this.userTheme = userTheme;
	}

	public String getSysMsgPosition() {
		return sysMsgPosition;
	}

	public void setSysMsgPosition(String sysMsgPosition) {
		this.sysMsgPosition = sysMsgPosition;
	}

	public int getTableSize() {
		return tableSize;
	}

	public void setTableSize(int tableSize) {
		this.tableSize = tableSize;
	}

	public String getDisplayName() {
		return displayName;
	}


}
