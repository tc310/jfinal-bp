package com.topteam.frame.entity;

import java.util.UUID;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.ModelBean;

@ModelBean("FrameMessage")
public class FrameMessage extends Model<FrameMessage>{

	private static final long serialVersionUID = -1440260127417764396L;
	
	private static final FrameMessage dao = new FrameMessage();
	
	public void newMsg(String content, String belongto){
		FrameMessage msg = new FrameMessage();
		msg.set("id", UUID.randomUUID().toString());
		msg.set("content", content);
		msg.set("belongto", belongto);
		msg.set("tag", "MSG");
		msg.save();
	}
}
