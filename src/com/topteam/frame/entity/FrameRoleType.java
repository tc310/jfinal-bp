package com.topteam.frame.entity;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.ModelBean;
import com.jfinal.plugin.activerecord.Page;

@ModelBean("FrameRoleType")
public class FrameRoleType extends Model<FrameRoleType> {

	private static final long serialVersionUID = -3743632753274581081L;
	public static final FrameRoleType dao = new FrameRoleType();

	public Page<FrameRoleType> getAllRoleType(int page, int size) {
		return paginate(page, size, "select t.*","from FrameRoleType t order by t.order desc");
	}
	
	public List<FrameRoleType> getAllRoleTypeNoPaginate(){
		return this.find("select t.* from FrameRoleType t order by t.order desc");
	}	
	public void deleteIds(String ids) {
		String[] id = ids.split(";");
		for (int i = 0; i < id.length; i++) {
			deleteById(id[i]);
		}
	}
}
