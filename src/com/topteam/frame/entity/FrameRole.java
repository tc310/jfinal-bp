package com.topteam.frame.entity;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.ModelBean;
import com.jfinal.plugin.activerecord.Page;

@ModelBean("FrameRole")
public class FrameRole extends Model<FrameRole> {

	private static final long serialVersionUID = -3743632753274581081L;
	public static final FrameRole dao = new FrameRole();

	public Page<FrameRole> getAllRole(int page, int size) {
		return paginate(page, size, "select t.*","from FrameRole t order by t.order desc");
	}
	
	public List<FrameRole> getAllRoleNoPaginate(){
		return find("select t.* from FrameRole t order by t.order desc");
	}
	
	public void deleteIds(String ids) {
		String[] id = ids.split(";");
		for (int i = 0; i < id.length; i++) {
			deleteById(id[i]);
		}
	}
	
	public List<FrameRole> getRolesByTypeId(String id){
		return find("select * from FrameRole where roleTypeId=?", id);
	}

	public List<FrameRole> queryRoleByUserId(String userid) {
		String sql = "select r.* from UserRoleRelation ur,FrameRole r where ur.roleId=r.id and ur.userId=?";
		return find(sql, userid);
	}
}
