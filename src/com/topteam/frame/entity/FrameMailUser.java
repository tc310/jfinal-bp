package com.topteam.frame.entity;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.ModelBean;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

@ModelBean("FrameMailUser")
public class FrameMailUser extends Model<FrameMailUser>{

	public final static FrameMailUser dao = new FrameMailUser();
	
	public Page<Record> getUserMail(String userId,int page,int size){
		String sql = "from FrameMailUser mu,FrameMail m where mu.mailId=m.id and mu.belongUser=? order by m.priority desc,m.sendTime desc";
		return Db.paginate(page, size, "select mu.*,m.id,m.title,m.sendUserName,m.sendTime,m.priority ", sql,userId);
	}

	public boolean queryAuth(String id, String userid) {
		String sql = "select count(1) from FrameMailUser where mailId=? and belongUser=?";
		return Db.queryLong(sql,id,userid)>0L;
	}
}
