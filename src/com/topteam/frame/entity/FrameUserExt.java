package com.topteam.frame.entity;

import java.util.UUID;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.ModelBean;


@ModelBean("FrameUserExt")
public class FrameUserExt extends Model<FrameUserExt>{

	private static final long serialVersionUID = -1351942220736287637L;

	public static final FrameUserExt dao = new FrameUserExt();

	public FrameUserExt getUserExtByUserId(String id) {
		String sql = "select * from FrameUserExt where userId = ?";
		return findFirst(sql, id);
	}
	
	public FrameUserExt initUserExt(String userId){
		FrameUserExt userExt = new FrameUserExt();
		userExt.set("userId", userId);
		userExt.set("id", UUID.randomUUID().toString());
		userExt.save();
		return userExt;
	}

	
}
