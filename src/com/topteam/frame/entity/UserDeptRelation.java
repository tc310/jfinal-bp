package com.topteam.frame.entity;


import java.util.UUID;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.ModelBean;
import com.jfinal.plugin.activerecord.Page;

@ModelBean("UserDeptRelation")
public class UserDeptRelation extends Model<UserDeptRelation>{

	private static final long serialVersionUID = -8123676378369371986L;
	
	public static UserDeptRelation dao=new UserDeptRelation();
	
	public Page getPage(){
		return paginate(1,0,"select t.deptname", " from FrameDept t where 1=0");
	}
	
	public Page getUserDeptRelationByUser(String userid,int isMain){
		return paginate(1,0,"SELECT t.id,t.deptid,d.deptName,t.dutyid,dd.dutyName"," from UserDeptRelation t LEFT JOIN FrameDept d on t.deptid=d.id  LEFT JOIN  FrameDeptDuty dd on t.dutyid=dd.id WHERE t.userid=? and t.isMainOu=?",userid,isMain);
	}
	
	public void addMainOu(String userid,String deptid,String isMainOu){
		String id=UUID.randomUUID().toString();
		String sql="insert into UserDeptRelation(id,userid,deptcode,order,isMainOu) values('"+id+"','"+userid+"','"+deptid+"',0,"+isMainOu+")";
		System.out.println(sql);
		Db.update(sql);
	}
	
	public UserDeptRelation getUserDeptRelation(String userid, String deptid){
		String sql = "select * from UserDeptRelation where userid=? and deptid=? ";
		return findFirst(sql, userid,deptid);
	}
	
	public UserDeptRelation getUserDeptRelation(String userid, String deptid,int isMain){
		String sql = "select * from UserDeptRelation where userid=? and deptid=? and isMainOu=?";
		return findFirst(sql, userid,deptid,isMain);
	}
	
	public void deleteUserDeptRelationByDutyId(String dutyId){
		String sql = "delete from UserDeptRelation where dutyid = ?";
		Db.update(sql, dutyId);
	}

	public void deleteUserDeptRelationByDetpId(String deptId) {
		String sql = "delete from UserDeptRelation where deptid = ?";
		Db.update(sql, deptId);
	}
}
