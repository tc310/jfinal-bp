package com.topteam.frame.entity;

import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.ModelBean;

@ModelBean("FrameAuth")
public class FrameAuth extends Model<FrameAuth>{

	public final static FrameAuth dao = new FrameAuth();

	public boolean exsitResourceRole(String resourceId, String roleId) {
		String sql = "select count(*) from FrameAuth where resource=? and grantTo = ?";
		return Db.queryLong(sql,resourceId,roleId)>0L;
	}
	
	public boolean isOpenAll(String resourceId){
		String sql = "select count(*) from FrameAuth where resource=? and openAll=1";
		return Db.queryLong(sql,resourceId)>0L;
	}

	public void deleteFrameAuthId(String id) {
		String sql = "delete from FrameAuth where resource=?";
		Db.update(sql,id);
	}
	
	public List<FrameAuth> queryRoleAuth(){
		String sql = "select * from FrameAuth where grantToType='ROLE'";
		return find(sql);
	}

	public List<FrameAuth> queryOpenAll() {
		String sql = "select * from FrameAuth where openAll=1";
		return find(sql);
	}
}
