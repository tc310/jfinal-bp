package com.topteam.frame.controllor;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.topteam.component.easyui.EasyDataTableModel;
import com.topteam.core.Path;
import com.topteam.frame.entity.FrameAuth;
import com.topteam.frame.entity.FrameMenu;
import com.topteam.frame.entity.FrameRole;
import com.topteam.security.Security;
import com.topteam.utility.StringUtil;

@Path("/frameauthorize")
public class FrameAuthorizeController extends BaseController{

	public void index(){
		List<FrameRole> list= FrameRole.dao.getAllRoleNoPaginate();
		setAttr("roles", list);
		render("/frame/FrameAuthorize.html");
	}
	
	@Security(depend="/index")
	public void authRoleTable(){
		EasyDataTableModel model = new EasyDataTableModel(this) {
			@Override
			public Page<Record> fechData(int page, int size) {
				String code= getPara("code");
				Page<Record> p = FrameMenu.dao.getMenuByCode(code,page,size);
				List<FrameRole> list= FrameRole.dao.getAllRoleNoPaginate();
				List<FrameAuth> roleAuth = FrameAuth.dao.queryRoleAuth();
				List<FrameAuth> openAll = FrameAuth.dao.queryOpenAll();
				for(int i=0,k=p.getList().size(); i<k;i++){
					Record r= p.getList().get(i);
					for(int j=0,l=list.size(); j<l;j++){
						FrameRole role = list.get(j);
						boolean b = exsitResourceRole(roleAuth,r.getStr("id"), role.getStr("id"));
						r.set(role.getStr("id"), b?"@"+role.getStr("id"):role.getStr("id"));
					}
					boolean f = exsitOpenAll(openAll,r.getStr("id"));
					r.set("OpenAll", f?r.getStr("id"):"");
				}
				return p;
			}
			
			private boolean exsitResourceRole(List<FrameAuth> roleAuth,String menuId,String roleId){
				for(FrameAuth auth:roleAuth){
					if(auth.getStr("resource").equals(menuId)&&auth.getStr("grantTo").equals(roleId))
						return true;
				}
				return false;
			}
			
			private boolean exsitOpenAll(List<FrameAuth> openAll,String menuId){
				for(FrameAuth auth:openAll){
					if(auth.getStr("resource").equals(menuId))
						return true;
				}
				return false;
			}
		};
		renderJson(model.toJson());
	}
	
	@Security(depend="/index")
	public void saveRoleAuth(){
		String roles = getPara("roles");
		String openall = getPara("openall");
		String ids = getPara("idlist");
		for(String id : ids.split(",")){
			if(StringUtil.isNotNull(id)){
				FrameAuth.dao.deleteFrameAuthId(id);
			}
		}
		
		String[] all = openall.split(",");
		String[] menuRoles = roles.split(",");
		List<String> allList = Arrays.asList(all);
		for(String ol : allList){
			if(StringUtil.isNotNull(ol)){
				FrameAuth auth = new FrameAuth();
				auth.set("id", UUID.randomUUID().toString());
				auth.set("resource", ol);
				auth.set("resourceType", "MENU");
				auth.set("openAll", 1);
				auth.save();
			}
		}
		for(String mr : menuRoles){
			if(StringUtil.isNotNull(mr)){
				String m = mr.split(":")[1];
				String r = mr.split(":")[0];
				if(!allList.contains(m)){
					FrameAuth auth = new FrameAuth();
					auth.set("id", UUID.randomUUID().toString());
					auth.set("resource", m);
					auth.set("resourceType", "MENU");
					auth.set("grantTo", r);
					auth.set("grantToType", "ROLE");
					auth.set("openAll", 0);
					auth.save();
				}
			}
		}
		renderText("保存成功");
	}
}
