package com.topteam.frame.controllor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.topteam.component.easyui.EasyDataTableModel;
import com.topteam.component.easyui.EasyTreeModel;
import com.topteam.component.easyui.EasyTreeNode;
import com.topteam.core.Path;
import com.topteam.frame.entity.cms.CmsCategory;
import com.topteam.frame.entity.cms.CmsNews;
import com.topteam.utility.StringUtil;

@Path("/framecms")
public class FrameCmsController extends BaseController{

	public void categorymanager(){
		render("/frame/cms/categorymanager.html");
	}
	
	public void categoryTree(){
		EasyTreeModel<CmsCategory> tree = new EasyTreeModel<CmsCategory>(this) {
			
			private static final long serialVersionUID = -1936250718915194126L;

			@Override
			public EasyTreeNode model2Node(CmsCategory t) {
				EasyTreeNode treeNode = new EasyTreeNode();
				treeNode.setId(t.getStr("id"));
				treeNode.setText(t.getStr("name"));
				Map<String, String> map = new HashMap<String, String>();
				map.put("code", t.getStr("code"));
				treeNode.setAttributes(map);
				treeNode.setHasChild(t.getLong("has")>0L);
				return treeNode;
			}
			
			@Override
			public List<CmsCategory> fechDate(String id) {
				String sql = "select t.*, (select count(1) from CmsCategory c where c.parentid=t.id) as has from CmsCategory t where 1=1 ";
				if(StringUtil.isNull(id)){
					sql += " and t.parentid is null order by t.order desc";
				}else{
					sql += " and t.parentid ='"+id+"' order by t.order desc";
				}
				List<CmsCategory> list = CmsCategory.dao.find(sql);
				return list;
			}
		};
		
		renderJson(tree.toJson());
	}
	
	public void categoryTable(){
		EasyDataTableModel table = new EasyDataTableModel(this) {
			@Override
			public Page<Record> fechData(int page, int size) {
				String code = getPara("code");
				return CmsCategory.dao.getCmsCategoryPage(code, page, size);
			}
		};
		renderJson(table.toJson());
	}
	
	public void saveCate(){
		CmsCategory category = new CmsCategory();
		category.set("id", UUID.randomUUID().toString());
		category.set("name",getPara("name"));
		category.set("shortname",getPara("shortname"));
		category.set("code",CmsCategory.dao.getNextCode(getPara("parentId")));
		category.set("parentid",StringUtil.isNotNull(getPara("parentId"))?getPara("parentId"):null);
		category.set("customcode",getPara("customcode"));
		category.set("order", getPara("order"));
		category.set("disable", getPara("disable"));
		category.save();
		renderText("保存成功");
	}
	
	public void delete(){
		String ids = getPara("ids");
		String[] idss = ids.split(";");
		for(String id :idss){
			if(StringUtil.isNotNull(id))
				CmsCategory.dao.deleteById(id);
		}
		renderText("保存成功");
	}
	
	public void updateCate(){
		CmsCategory category = CmsCategory.dao.findById(getPara("id"));
		String parentId = getPara("parentId");
		parentId = StringUtil.isNotNull(parentId)?parentId:null;
		boolean flag = false;
		if(category.get("parentid") == null ){
			if(parentId!=null)
				flag = true;
		}else{
			if(!category.get("parentid").equals(parentId))
				flag = true;
		}
		category.set("name",getPara("name"));
		category.set("shortname",getPara("shortname"));
		if(flag)
			category.set("code",CmsCategory.dao.getNextCode(getPara("parentId")));
		category.set("parentid",StringUtil.isNotNull(getPara("parentId"))?getPara("parentId"):null);
		category.set("customcode",getPara("customcode"));
		category.set("order", getPara("order"));
		category.set("disable", getPara("disable"));
		category.update();
		renderText("保存成功");
	}
	
	public void newslist(){
		render("/frame/cms/newsmanager.html");
	}
	
	public void newsTable(){
		EasyDataTableModel table = new EasyDataTableModel(this) {
			@Override
			public Page<Record> fechData(int page, int size) {
				String cateid = getPara("cateid");
				return CmsNews.dao.getNewsPage(cateid, page, size);
			}
		};
		renderJson(table.toJson());
	}
}
