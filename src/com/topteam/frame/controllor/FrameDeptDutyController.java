package com.topteam.frame.controllor;

import java.util.UUID;

import com.jfinal.plugin.activerecord.Page;
import com.topteam.component.easyui.EasyDataTableModel;
import com.topteam.core.Path;
import com.topteam.frame.entity.FrameDeptDuty;
import com.topteam.frame.entity.UserDeptRelation;
import com.topteam.security.Security;
import com.topteam.utility.StringUtil;

@Path("/frameduty")
public class FrameDeptDutyController extends BaseController{

	
	public void index(){
		render("/frame/FrameDeptDuty.html");
	}
	
	public void save(){
		FrameDeptDuty duty = getModel(FrameDeptDuty.class,"frameduty");
		if(StringUtil.isNull(duty.getStr("deptId"))){
			renderText("请选择部门");
			return;
		}
		duty.set("id", UUID.randomUUID().toString());
		duty.save();
		renderText("添加成功");
	}
	
	@Security(depend="/index")
	public void table(){
		EasyDataTableModel model = new EasyDataTableModel(this) {
			@Override
			public Page<FrameDeptDuty> fechData(int page, int size) {
				String id = getPara("id");
				Page<FrameDeptDuty> p;
				if(StringUtil.isNotNull(id)){
					p = FrameDeptDuty.dao.paginate(page, size, "select t.*,d.deptName", "from FrameDept d,FrameDeptDuty t where t.deptId=d.id and d.id=? order by t.order desc",id);
				}else{
					p = FrameDeptDuty.dao.paginate(page, size, "select t.*,d.deptName", "from FrameDept d,FrameDeptDuty t where t.deptId=d.id order by t.order desc");
				}
				return p;
			}
		};
		
		renderJson(model.toJson());
	}
	
	public void delete(){
		String id = getPara("id");
		if(StringUtil.isNotNull(id)){
			FrameDeptDuty.dao.deleteById(id);
			UserDeptRelation.dao.deleteUserDeptRelationByDutyId(id);
		}
		redirect("/frameduty/index");
	}
}
