package com.jfinal.plugin.activerecord;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target(ElementType.TYPE)  
@Retention(RetentionPolicy.RUNTIME)  
public @interface ModelBean {
	String value();
}
